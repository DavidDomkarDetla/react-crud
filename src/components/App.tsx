import React, { useState } from 'react';
import { Theme as UWPThemeProvider, getTheme } from 'react-uwp/Theme';
import TransformCard from 'react-uwp/TransformCard';
import './App.css';
import ItemForm from './ItemForm';
import Header from './Header';
import ItemTable from './ItemsTable';
import MobilePhone from '../entities/mobile-phone';

function App() {
  const [items, setItems] = useState<Array<MobilePhone>>([
    {
      company: 'Apple',
      model: 'iPhone 11',
    },
    {
      company: 'Apple',
      model: 'iPhone 11 Pro',
    },
    {
      company: 'Apple',
      model: 'iPhone 11 Pro Max',
    },
    {
      company: 'Samsung',
      model: 'Galaxy S20',
    },
    {
      company: 'Samsung',
      model: 'Galaxy S20+',
    },
    {
      company: 'Samsung',
      model: 'Galaxy S20 Ultra',
    },
  ]);

  return (
    <UWPThemeProvider
      theme={getTheme({
        themeName: 'dark',
        accent: '#00ffff',
        useFluentDesign: true,
      })}
    >
      <div className="App">
        <TransformCard perspective={1000}>
          <div className="App-content">
            <Header title="React CRUD - Mobile phones" />
            <ItemForm
              onAddItem={item => {
                setItems([...items, item]);
              }}
            />
            <Header title="Items" />
            <ItemTable
              items={items}
              onDeleteItemAtIndex={index => {
                items.splice(index, 1);
                setItems([...items]);
              }}
            />
          </div>
        </TransformCard>
      </div>
    </UWPThemeProvider>
  );
}

export default App;
