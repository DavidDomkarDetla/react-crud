import React from 'react';
import MobilePhone from '../entities/mobile-phone';
import Button from 'react-uwp/Button';
import ListView from 'react-uwp/ListView';

import './ItemsTable.css';

type Props = {
  items: Array<MobilePhone>;
  onDeleteItemAtIndex: (index: number) => void;
};

function ItemsTable({ items, onDeleteItemAtIndex }: Props) {
  return (
    <ListView
      style={{ width: '100%' }}
      listSource={[
        <div className="ItemsTable-row">
          <div className="ItemsTable-cell">
            <h3>ID</h3>
          </div>
          <div className="ItemsTable-cell">
            <h3>Company</h3>
          </div>
          <div className="ItemsTable-cell">
            <h3>Model</h3>
          </div>
          <div className="ItemsTable-cell"></div>
        </div>,
        ...items.map((item, index) => (
          <div className="ItemsTable-row" key={index}>
            <div className="ItemsTable-cell">
              <span>{index + 1}</span>
            </div>
            <div className="ItemsTable-cell">
              <span>{item.company}</span>
            </div>
            <div className="ItemsTable-cell">
              <span>{item.model}</span>
            </div>
            <div className="ItemsTable-cell">
              <Button
                className="ItemForm-button"
                onClick={() => onDeleteItemAtIndex(index)}
              >
                Delete
              </Button>
            </div>
          </div>
        )),
      ]}
    />
  );
}

export default ItemsTable;
