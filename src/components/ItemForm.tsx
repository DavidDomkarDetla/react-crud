import React, { useState, useRef } from 'react';

import TextBox from 'react-uwp/TextBox';
import Button from 'react-uwp/Button';

import './ItemForm.css';
import MobilePhone from '../entities/mobile-phone';

type Props = {
  onAddItem: (item: MobilePhone) => void;
};

function ItemForm({ onAddItem }: Props) {
  const [company, setCompany] = useState('');
  const [model, setModel] = useState('');

  const companyTextBox = useRef<TextBox>(null);
  const modelTextBox = useRef<TextBox>(null);

  return (
    <div className="ItemForm">
      <h2 className="ItemForm-title">Add new item:</h2>
      <div className="ItemForm-field">
        <TextBox
          ref={companyTextBox}
          placeholder="Company"
          onChangeValue={company => setCompany(company)}
        />
      </div>
      <div className="ItemForm-field">
        <TextBox
          ref={modelTextBox}
          placeholder="Model"
          onChangeValue={model => setModel(model)}
        />
      </div>
      <Button
        className="ItemForm-button"
        onClick={() => {
          if (company.length > 0 && model.length > 0) {
            companyTextBox.current?.setValue('');
            modelTextBox.current?.setValue('');

            setCompany('');
            setModel('');

            onAddItem({
              company,
              model,
            });
          }
        }}
      >
        Add
      </Button>
    </div>
  );
}

export default ItemForm;
