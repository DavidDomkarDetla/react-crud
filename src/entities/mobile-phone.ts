export default interface MobilePhone {
  company: string;
  model: string;
}
